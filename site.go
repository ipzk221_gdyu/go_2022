package main

import (
	"fmt"
	"html/template"
	"net/http"
)

var tpl *template.Template
var Username string = "root"
var Password string = "root"

func index(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/index.html")
	if err != nil {
		fmt.Print(err.Error())
	}
	t.ExecuteTemplate(w, "index.html", nil)
}

func submit(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/signin.html")
	if err != nil {
		fmt.Print(err.Error())
	}
	if r.Method != "POST" {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

	username := r.FormValue("username")
	password := r.FormValue("password")

	if username != Username {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	if password != Password {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	x := struct {
		Username string
		Password string
		Author   string
	}{
		Username: username,
		Password: password,
		Author:   "Denis Andriyovych Pilov",
	}

	t.ExecuteTemplate(w, "signin.html", x)
}

func main() {
	http.Handle("/assets", http.StripPrefix("/assets/", http.FileServer(http.Dir("/assets"))))
	http.Handle("/", http.FileServer(http.Dir("templates")))
	http.HandleFunc("/signin", submit)

	port := ":3100"
	http.ListenAndServe(port, nil)

}
